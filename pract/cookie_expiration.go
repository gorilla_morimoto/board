// 有効期限1時間のクッキーを設定する
// one-hour-cookie-in-go.md

package pract

import (
	"fmt"
	"net/http"
	"time"
)

func cookie() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		timeUTC := time.Now().In(time.UTC).Add(time.Duration(1 * time.Hour))
		timeLocal := time.Now().Add(time.Duration(1 * time.Hour))

		fmt.Println("timeUTC:", timeUTC)
		fmt.Println("timeLocal:", timeLocal)

		anHour := 60 * 60

		c1 := http.Cookie{
			Name:    "hoge",
			Value:   "in UTC",
			Expires: timeUTC,
		}

		c2 := http.Cookie{
			Name:    "mogu",
			Value:   "in local_time",
			Expires: timeLocal,
		}

		c3 := http.Cookie{
			Name:   "foo",
			Value:  "bar",
			MaxAge: anHour,
		}

		http.SetCookie(w, &c1)
		http.SetCookie(w, &c2)
		http.SetCookie(w, &c3)
	})

	http.ListenAndServe(":9000", nil)
}

package pract

import (
	"log"
	"net/http"
)

func getReq() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("RemoteAddr:", r.RemoteAddr)
		log.Println("RequestURI:", r.RequestURI)
		log.Println("URL.Path:", r.URL.Path)
		log.Println("URL.RawPath:", r.URL.RawPath)
	})

	http.ListenAndServe(":8000", nil)
}

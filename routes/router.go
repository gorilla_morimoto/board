package routes

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/gorilla_morimoto/board/controller"
)

// RunServer はサーバーを起動する関数
func RunServer() {
	http.HandleFunc("/", controller.Post)
	http.HandleFunc("/delete", controller.Delete)

	http.Handle("/favicon.ico", http.FileServer(http.Dir("./views/assets/")))

	port := getPort()

	go http.ListenAndServe(":"+port, nil)

	log.Println("DEBUG: listen on", port)

}

func getPort() string {
	if os.Getenv("PORT") == "" {
		return "8000"
	}
	return os.Getenv("PORT")
}

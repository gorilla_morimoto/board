package controller

import (
	"crypto/sha256"
	"fmt"
	"log"
	"net/http"
	"strings"
)

func isValidTrackingID(r *http.Request) bool {
	trackingID, err := r.Cookie("tracking_id")
	// tracking_idが設定されていない
	if err != nil {
		log.Println("ERROR: tracking_id is empty")
		return false
	}

	requestedTrackingID := strings.Split(trackingID.Value, "_")
	// requestedTrackingIDは[もとのtracking_id, ハッシュ]になっているはず
	if len(requestedTrackingID) != 2 {
		log.Println("ERROR: length of splitted result is not 2 =>", requestedTrackingID)
		return false
	}

	hash := fmt.Sprintf("%x", sha256.Sum256([]byte(requestedTrackingID[0]+"_"+getIPAddress(r))))

	if requestedTrackingID[1] != hash {
		log.Println("DEBUG: requestedTrackingID =>", requestedTrackingID[1])
		log.Println("DEBUG: request ip address =>", getIPAddress(r))
		log.Println("DEBUG: hash =>", hash)
		return false
	}

	return true
}

// addTrackingID はリクエストのtracking_idを見て
// 空なら新たに設定し、値が設定されていたら何もしません
// また、最終的に設定されているtrackind_idの値を返します
func addTrackingID(w http.ResponseWriter, r *http.Request) string {
	// "tracking_id"というCookieを取得できなかった場合に
	// エラーになるっぽい（たぶん）のでエラーの値は取得せずに
	// 下で空文字かどうかを判別して処理します

	// と思ってエラーを捨てたところ、c,Valueを取得するところで「にるぽ」が発生しました
	// "tracking_id"が設定されていないときの処理は
	// c.Valueの空文字比較ではなくCookieを取得する際のエラーで判断するべきみたいです
	c, err := r.Cookie("tracking_id")

	// err != nil は"tracking_id"が設定されていない場合
	if err != nil {
		token, err := generateRandomToken()
		// エラーが出るのはシステムの乱数にアクセスできなかったときなので、
		// リトライすれば正常に値が取れる可能性があると思ってこうしました
		if err != nil {
			token, _ = generateRandomToken()
		}
		// トークンに_IPアドレスを追加してハッシュしたものに、再度トークン_を追加する
		trackingID := token + "_" + fmt.Sprintf("%x", sha256.Sum256([]byte(token+"_"+getIPAddress(r))))
		log.Println("DEBUG: generate tracking ID =>", trackingID)

		http.SetCookie(w, &http.Cookie{
			Name:  "tracking_id",
			Value: trackingID,
			// IEはmax-ageをサポートしていないという情報がありましたが、
			// IEを使う予定はない、本来はこちらを使うべき（らしい）
			// という理由からMaxAgeのみを記述しています
			MaxAge: 86400, //60 * 60 * 24
		})
		return trackingID
	}
	return c.Value
}

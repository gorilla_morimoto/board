package controller

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/gorilla_morimoto/board/models"
)

func createPost(userName, content, trackingID string, date time.Time) error {
	if len(content) > 2049 {
		return fmt.Errorf("too long post")
	}

	if userName == "" {
		userName = "名無し"
	}

	db, err := models.WhichDB()
	defer db.Close()
	if err != nil {
		log.Println("FATAL: failed to connect to DB =>", err)
		return errors.New("DB-CONNECTION")
	}

	stmt, err := db.Prepare("insert into test(user_names, contents,tracking_id, date) values($1, $2, $3, $4)")
	if err != nil {
		log.Println("FATAL: failed to create prepare statement =>", err)
		return err
	}

	_, err = stmt.Exec(userName, content, trackingID, date)
	if err != nil {
		log.Println("FATAL: failed to exec sql =>", err)
		return err
	}

	go log.Println("DEBUG: successfuly saved")
	return nil
}

func getPosts() ([]models.Post, error) {
	db, err := models.WhichDB()
	defer db.Close()

	if err != nil {
		log.Println("FATAL: failed to connect to DB =>", err)
		return nil, err
	}
	// tracking_idは長すぎてレイアウトを崩すため
	// left()を使っています
	rows, err := db.Query(`select id, user_names, contents, to_char(date, 'YYYY/MM/DD (Dy) HH24:MI:SS'), left(tracking_id, 8) from test`)
	if err != nil {
		log.Println("ERROR: failed to get posts")
		return nil, err
	}

	// ここで初期の長さを設定してしまうと
	// その数だけ空のmodels.Postが作成されてしまい、
	// テンプレート側でrangeしたときに空のPost分だけ
	// 余計にループしてしまう（{{}}に囲まれていない文字がその分余計に表示される）ため
	// 初期の長さをは0にしてあります。
	ps := make([]models.Post, 0)
	// ps := make([]models.Post, 20, 1000)
	for rows.Next() {
		var p models.Post
		err := rows.Scan(&p.ID, &p.Username, &p.Content, &p.Date, &p.TrackingID)
		if err != nil {
			log.Println("ERROR: failed to scan", err)
			return nil, err
		}
		ps = append(ps, p)
	}

	return ps, nil
}

func deletePost(postID string) error {
	db, err := models.WhichDB()
	defer db.Close()
	if err != nil {
		log.Println("FATAL: failed to connect to DB =>", err)
		return err
	}

	res, err := db.Exec(`delete from test where id=$1`, postID)
	if err != nil {
		log.Println("ERROR: failed to delete post => postID:", postID)
		return err
	}
	deleted, _ := res.RowsAffected()
	if deleted != 1 {
		log.Println("ERROR: multiple posts may have been deleted => deleted number:", deleted)
	}

	return nil
}
